package org.cbase.trackme.gae;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Enumeration;
import java.util.List;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.cbase.trackme.gae.model.Strength;

/**
 * A helper class to map api requests into the internal representation.
 * 
 * @author mmachicao
 * 
 */
public class APICallTranslator {

	private static final Logger log = Logger.getLogger(APICallTranslator.class
			.getName());
	
	
	interface ValidSaveLocationArgs {
	
		String TIMESTAMP = "timestamp";
		
		String LOCATION = "location";
	}
	
	interface SupportedTypePrefixes {

		String MAC = "mac";

		String GSM = "gsm";

		String UMTS = "umts";

	}


	/**
	 * Parses non sample arguments from the request object.
	 * 
	 * @param req
	 * @param resp
	 * @return
	 * @throws IOException
	 */
	public static SaveLocationArguments parsteSaveArguments(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		
		/*
		 * Set timestamp as provided by the caller.
		 * 
		 * Default timestamp is the server date.
		 */
		Date timeStamp = new Date();
		
		String incoming = (req.getParameter(ValidSaveLocationArgs.TIMESTAMP) != null ? req
				.getParameter(ValidSaveLocationArgs.TIMESTAMP) : "");

		if (incoming.length() > 0) {

			try {
				timeStamp = new Date(Long.parseLong(incoming));
				log.finer("Timestamp (user): " + timeStamp);

			} catch (NumberFormatException e) {
				Response response = new Response(Response.Status.ERROR,
						"invalid timestamp specified");
				resp.getWriter().print(response.toJson());
				return null;
			}

		} else {
			log.finer("Timestamp (server): " + timeStamp);
		}

		// location is a mandatory parameter..
		String location = req.getParameter(ValidSaveLocationArgs.LOCATION);
		if (location == null || location.length() == 0) {
			Response response = new Response(Response.Status.ERROR,
					"location is mandatory");
			resp.getWriter().print(response.toJson());
			return null;
		}

		return null;
	}
	
	/**
	 * 
	 * Parse available samples from the request object
	 * 
	 * 
	 * param req
	 * @param resp
	 * @return
	 * 
	 * 
	 * 
	 */
	public static List<Strength> parseSamples(HttpServletRequest req, HttpServletResponse resp) {

		List<Strength> listSamples = new ArrayList<Strength>();

		@SuppressWarnings("unchecked")
		Enumeration<String> addressList = req.getParameterNames();

		while (addressList.hasMoreElements()) {
			String address = addressList.nextElement();

			String coreAddress = null;

			if (address.startsWith(SupportedTypePrefixes.MAC)) {
				coreAddress = address.substring(SupportedTypePrefixes.MAC
						.length());
			} else if (address.startsWith(SupportedTypePrefixes.GSM)) {
				coreAddress = address.substring(SupportedTypePrefixes.GSM
						.length());
			} else if (address.startsWith(SupportedTypePrefixes.UMTS)) {
				coreAddress = address.substring(SupportedTypePrefixes.UMTS
						.length());
			}

			if (coreAddress == null) {
				log.finer("unsupported address: " + address);
				continue;
			}

			int signal = 0;
			try {
				signal = Integer.parseInt(req.getParameter(address));
			} catch (NumberFormatException e) {
				Response response = new Response(Response.Status.ERROR,
						"invalid signal strength specified: "
								+ req.getParameter(address));

				try {
					resp.getWriter().print(response.toJson());
				} catch (IOException ex) {
					log.finer(ex.getMessage());
				}
				return null;
			}

			Strength so = new Strength();
			so.setAddress(coreAddress);
			so.setStrength(signal);

			listSamples.add(so);

		}

		return listSamples;
	}

}
