package org.cbase.trackme.gae.dao;

import java.util.List;



/**
 *  Relationship between an address and the list of fingerprints
 *  where that particular address is present.
 *  
 * @author mmachicao
 *
 */
public class Address2Finger implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String address;
	
	private List<Long> fingerIds;

	/**
	 * Default constructor
	 * 
	 */
	public Address2Finger() {
		super();
	}

	

	public String getAddress() {
		return address;
	}



	public void setAddress(String address) {
		this.address = address;
	}



	public List<Long> getFingerIds() {
		return fingerIds;
	}

	public void setFingerIds(List<Long> fingerIds) {
		this.fingerIds = fingerIds;
	}
	
	
	
}
