package org.cbase.trackme.gae.utils;

import java.util.Comparator;

/**
 * Compare between confidence values assigned to a fingerprint
 * 
 * User: biafra
 * Date: 1/4/12
 * Time: 10:37 PM
 * 
 */
public class ConfidenceComparator implements Comparator<ConfidenceFingerprintPair> {


	/**
	 * Compare the values of confidence only. greater confidence is better.
	 */
  @Override
  public int compare(ConfidenceFingerprintPair confidenceFingerprintPair, ConfidenceFingerprintPair confidenceFingerprintPair1) {

    if (confidenceFingerprintPair.getConfidence() == confidenceFingerprintPair1.getConfidence()) {
      return 0;
    }
    if (confidenceFingerprintPair.getConfidence() < confidenceFingerprintPair1.getConfidence()) {
      return 1;
    }

    return -1;
  }
}
