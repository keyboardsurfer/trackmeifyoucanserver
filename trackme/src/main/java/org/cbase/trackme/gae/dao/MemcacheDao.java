package org.cbase.trackme.gae.dao;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Logger;

import net.sf.jsr107cache.Cache;
import net.sf.jsr107cache.CacheException;
import net.sf.jsr107cache.CacheFactory;
import net.sf.jsr107cache.CacheManager;

import org.cbase.trackme.gae.db.Dao;
import org.cbase.trackme.gae.model.Fingerprint;
import org.cbase.trackme.gae.model.Strength;

import com.googlecode.objectify.Key;
import com.googlecode.objectify.Objectify;
import com.googlecode.objectify.ObjectifyService;

public class MemcacheDao implements Dao {

	private static final Logger log = Logger.getLogger(MemcacheDao.class
			.getName());

	private static Cache cache;

	static {
		ObjectifyService.register(Finger.class);
		ObjectifyService.register(Sample.class);
	}

	private static Objectify getService() {
		return ObjectifyService.begin();
	}

	private static Cache getCache() {

		if (cache == null)
			try {
				CacheFactory cacheFactory = CacheManager.getInstance()
						.getCacheFactory();
				cache = cacheFactory.createCache(Collections.emptyMap());

			} catch (CacheException e) {
				log.info("Faild to create an memcache instance: "
						+ e.getMessage());
			}

		return cache;
	}

	@Override
	public long insert(Fingerprint fingerprint) {

		Cache cc = getCache();

		/*
		 * split into finger and sample and persist... cache fingerprint and
		 * finally, update the mac registry
		 */
		Finger f = new Finger();
		f.setLocation(fingerprint.getLocation());
		f.setTimestamp(fingerprint.getTimeStamp());

		Key<Finger> key = getService().put(f);

		Long fingerId = new Long(key.getId());
		fingerprint.setId(fingerId);

		cc.put(fingerprint.getId(), fingerprint);

		// persist each sample...
		for (Strength st : fingerprint.getStrengths()) {
			Sample sp = new Sample();

			String address = st.getAddress();
			sp.setFingerId(fingerId);
			sp.setAddress(address);
			sp.setStrength(st.getStrength());

			Key<Sample> k = getService().put(sp);

			log.info("persisted sample with id: " + k.getId());

			// update the fingerprint lists in the cache...
			Address2Finger mfp;
			Object obj = cc.get(address);

			if (obj != null) {
				mfp = (Address2Finger) obj;
			} else {
				mfp = new Address2Finger();
				mfp.setAddress(address);
			}

			if (mfp.getFingerIds() != null) {
				mfp.getFingerIds().add(fingerId);
			} else {
				List<Long> l = new ArrayList<Long>();
				l.add(fingerId);
				mfp.setFingerIds(l);
			}

			cc.put(address, mfp);

		}

		return key.getId();
	}

	@Override
	public Fingerprint findFingerprintForId(long id) {

		Cache cc = getCache();

		// if found, return
		Object obj = cc.get(new Long(id));

		if (obj != null) {
			return (Fingerprint) obj;
		}

		// if not found, query db and eventually, refresh into cache

		// query finger and samples...
		Finger fp = getService().get(Finger.class, id);

		Collection<Sample> sl = getService().query(Sample.class)
				.filter("fingerId =", id).list();

		List<Strength> strengths = new ArrayList<Strength>();

		for (Sample s : sl) {
			Strength str = new Strength(s.getAddress(), s.getStrength());
			strengths.add(str);
		}

		Fingerprint f = new Fingerprint();
		f.setId(fp.getId());
		f.setLocation(fp.getLocation());
		f.setTimeStamp(fp.getTimestamp());
		f.setStrengths(strengths);

		// put into the cache
		cc.put(f.getId(), f);

		return f;
	}

	@Override
	public Collection<Fingerprint> getFingerprints() {
		// TODO Auto-generated method stub

		Collection<Fingerprint> fs = new ArrayList<Fingerprint>();

		Collection<Finger> all = getService().query(Finger.class).list();

		for (Finger f : all) {
			Fingerprint fp = this.findFingerprintForId(f.getId());
			fs.add(fp);
		}

		return fs;
	}

	@Override
	public Collection<Fingerprint> getMatchingFingerprints(
			List<Strength> requestStrengths) {

		Cache cc = getCache();

		/*
		 * Get samples for all macs in the list and collect all finger ids into
		 * a list
		 */

		Set<Long> fingerIds = new HashSet<Long>();

		for (Strength s : requestStrengths) {

			Address2Finger mfp;

			String address = s.getAddress();
			Object obj = cc.get(address);

			if (obj != null) { // cache knows something...
				mfp = (Address2Finger) obj;

				if (mfp.getFingerIds() != null) {
					for (Long id : mfp.getFingerIds()) {
						fingerIds.add(id);
					}
				}

			} else { // need to look in the database and store the result in the
						// cache...

				Collection<Sample> sl = getService().query(Sample.class)
						.filter("mac =", address).list();

				List<Long> ids = new ArrayList<Long>();

				for (Sample sm : sl) {

					fingerIds.add(sm.getFingerId());
					ids.add(sm.getFingerId());

				}

				mfp = new Address2Finger();
				mfp.setAddress(address);
				mfp.setFingerIds(ids);

				cc.put(mfp.getAddress(), mfp);

			}

		}

		/*
		 * Load all fingerprints referenced in the list
		 */

		Collection<Fingerprint> fs = new ArrayList<Fingerprint>();

		for (Long id : fingerIds) {
			fs.add(this.findFingerprintForId(id));
		}

		return fs;
	}

}
