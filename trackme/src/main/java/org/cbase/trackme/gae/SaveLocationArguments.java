package org.cbase.trackme.gae;

import java.util.Date;

public class SaveLocationArguments {

	Date timestamp;
	
	String position;

	public SaveLocationArguments(Date timestamp, String position) {
		super();
		this.timestamp = timestamp;
		this.position = position;
	}

	public Date getTimestamp() {
		return timestamp;
	}

	public String getPosition() {
		return position;
	}
	
	
}
