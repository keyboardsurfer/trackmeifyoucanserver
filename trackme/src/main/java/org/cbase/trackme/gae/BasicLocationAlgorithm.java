package org.cbase.trackme.gae;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.logging.Logger;

import org.cbase.trackme.gae.dao.MemcacheDao;
import org.cbase.trackme.gae.db.Dao;
import org.cbase.trackme.gae.model.Fingerprint;
import org.cbase.trackme.gae.model.Strength;
import org.cbase.trackme.gae.utils.ConfidenceComparator;
import org.cbase.trackme.gae.utils.ConfidenceFingerprintPair;
import org.cbase.trackme.gae.utils.AddressComparator;

/**
 * Basic implementation of a location algorithm.
 * 
 * @author mmachicao
 * 
 */
public class BasicLocationAlgorithm implements LocationRequestAlgorithm {

	private static final Logger log = Logger
			.getLogger(BasicLocationAlgorithm.class.getName());

	private Dao dao = new MemcacheDao();

	@Override
	public List<ConfidenceFingerprintPair> sortedMatchingFingerprintsL(
			List<Strength> requestStrengths) {
		AddressComparator addressComparator = new AddressComparator();

		Collections.sort(requestStrengths, addressComparator);  // not necessary...

		Collection<Fingerprint> entries = dao
				.getMatchingFingerprints(requestStrengths);

		List<ConfidenceFingerprintPair> result = computeConfidence(
				requestStrengths, entries);

		return result;
	}

	protected List<ConfidenceFingerprintPair> computeConfidence(
			List<Strength> requestStrengths, Collection<Fingerprint> entries) {

		
		log.finer("Found " + entries.size() + " Fingerprint entries");
		
		float lastConfidence = Float.MAX_VALUE;

		AddressComparator macComparator = new AddressComparator();
		
		Collections.sort(requestStrengths, macComparator);  // really necessary?
		
		ArrayList<ConfidenceFingerprintPair> result = new ArrayList<ConfidenceFingerprintPair>();

		// no further processing, if no fingerprints found...
		if (entries == null || entries.isEmpty()) {
			return result;
		}

		for (Fingerprint fingerprint : entries) {

			final List<Strength> strengths = fingerprint.getStrengths();

			Collections.sort(strengths, macComparator); // is sorting relevant?

			float confidence = BasicLocationAlgorithm.compareStrength(
					strengths, requestStrengths);  // i assume, confidence is on relative distance...

			if (confidence < lastConfidence) {
				lastConfidence = confidence;
			}
			result.add(new ConfidenceFingerprintPair(confidence, fingerprint));
		}

		Collections.sort(result, new ConfidenceComparator());
		return result;

	}

	static class AddressStrengthPair {
		public String address;
		public int strength1;
		public int strength2;

		public AddressStrengthPair() {
		}

		public AddressStrengthPair(String mac, int s1, int s2) {
			this.address = mac;
			this.strength1 = s1;
			this.strength2 = s2;
		}

		@Override
		public String toString() {
			return "AddressStrengthPair{" + "address='" + address + '\'' + ", strength1="
					+ strength1 + ", strength2=" + strength2 + '}';
		}
	}

	public static float compareStrength(List<Strength> list1,
			List<Strength> list2) {

		List<BasicLocationAlgorithm.AddressStrengthPair> macStrengthPairs = BasicLocationAlgorithm
				.mergeStrengths(list1, list2);

		if (list1 == null || list2 == null) {
			return 100;
		}
		int diffSum = 0;
		int maxCount = macStrengthPairs.size();
		if (maxCount == 0)
			return 100;

		for (BasicLocationAlgorithm.AddressStrengthPair pair : macStrengthPairs) {
			int diff = Math.abs(pair.strength1 - pair.strength2);
			diffSum += diff;
		}

		return (float) diffSum / maxCount;
	}

	public static List<BasicLocationAlgorithm.AddressStrengthPair> mergeStrengths(
			List<Strength> list1, List<Strength> list2) {

		List<BasicLocationAlgorithm.AddressStrengthPair> result = new ArrayList<BasicLocationAlgorithm.AddressStrengthPair>();
		int size1;
		int size2;
		if (list1 == null) {
			size1 = 0;
		} else {
			size1 = list1.size();
		}
		if (list2 == null) {
			size2 = 0;
		} else {
			size2 = list2.size();

		}
		int i1 = 0;
		int i2 = 0;
		while (i1 < size1 || i2 < size2) {
			Strength strengthPair1 = null;
			Strength strengthPair2 = null;
			if (i1 < size1) {
				strengthPair1 = list1.get(i1);
			}
			if (i2 < size2) {
				strengthPair2 = list2.get(i2);
			}

			String address= null;
			String address1 = null;
			String address2 = null;

			int strength1 = 100;
			int strength2 = 100;

			if (strengthPair1 != null) {
				address1 = strengthPair1.getAddress();

			}
			if (strengthPair2 != null) {
				address2 = strengthPair2.getAddress();
			}

			if (address1 != null) {
				if (address2 != null) {

					if (address1.equals(address2)) {

						strength1 = strengthPair1.getStrength();
						strength2 = strengthPair2.getStrength();
						i1++;
						i2++;
						address = address1;

					} else if (address1.compareTo(address2) < 0) {

						strength1 = strengthPair1.getStrength();
						address = address1;
						i1++;

					} else if (address1.compareTo(address2) > 0) {

						strength2 = strengthPair2.getStrength();
						address = address2;
						i2++;

					}
				} else {

					strength1 = strengthPair1.getStrength();
					i1++;

					address = address1;
				}

			} else {
				if (address2 == null) {
					i1++;
					i2++;
				} else {

					strength2 = strengthPair2.getStrength();
					address = address2;
					i2++;

				}
			}

			BasicLocationAlgorithm.AddressStrengthPair pair = new BasicLocationAlgorithm.AddressStrengthPair(
					address, strength1, strength2);
			result.add(pair);

		}// done

		return result;
	}

}
