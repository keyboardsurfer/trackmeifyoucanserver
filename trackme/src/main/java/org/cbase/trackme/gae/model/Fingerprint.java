package org.cbase.trackme.gae.model;

import java.util.Date;
import java.util.List;

import javax.persistence.Embedded;
import javax.persistence.Id;

import com.google.gson.FieldNamingPolicy;
import com.google.gson.GsonBuilder;
import com.google.gson.annotations.Expose;
import com.googlecode.objectify.annotation.Cached;

/**
 * Association betwen a room name and a set of samples that denote a fingerprint
 * 
 * User: biafra
 * Date: 12/29/11
 * Time: 10:39 PM
 */
@Cached
public class Fingerprint implements java.io.Serializable {
	

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Unique id of this fingerprint
	 */
  @Id
  Long id;
  
  /**
   * Associates this fingerprint to a name
   * 
   * 
   */
  @Expose
  String location;
  
  /**
   * denotes when the fingerprint was sampled
   */
  Date timeStamp;

  /**
   * list of samples detected by the sampling device
   */
  @Embedded
  List<Strength> strengths;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getLocation() {
    return location;
  }

  public void setLocation(String location) {
    this.location = location;
  }

  public Date getTimeStamp() {
    return timeStamp;
  }

  public void setTimeStamp(Date timeStamp) {
    this.timeStamp = timeStamp;
  }

  public List<Strength> getStrengths() {
    return strengths;
  }

  public void setStrengths(List<Strength> strengths) {
    this.strengths = strengths;
  }

  @Override
  public String toString() {
    return "Fingerprint{" +
           "id=" + id +
           ", position='" + location + '\'' +
           ", timeStamp=" + timeStamp +
           ", strengths=" + strengths +
           '}';
  }

  public String toJsonObject() {

    GsonBuilder builder = new GsonBuilder().setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES);
    return builder.create().toJson(this);
  }

}
