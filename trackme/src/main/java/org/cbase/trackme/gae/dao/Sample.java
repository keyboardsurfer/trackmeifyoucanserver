package org.cbase.trackme.gae.dao;

import java.io.Serializable;

import javax.persistence.Id;

public class Sample implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	private Long id;
	
	private String address;
	
	private Integer strength;
	
	private Long fingerId;

	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public Integer getStrength() {
		return strength;
	}

	public void setStrength(Integer strength) {
		this.strength = strength;
	}

	public Long getFingerId() {
		return fingerId;
	}

	public void setFingerId(Long fingerId) {
		this.fingerId = fingerId;
	}
	
	
}
