package org.cbase.trackme.gae;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Enumeration;
import java.util.List;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.cbase.trackme.gae.model.Strength;

/**
 * A helper class to provide helper methods for the validation of input
 * 
 * @author mmachicao
 * 
 */
public class RequestHelper {

	private static final Logger log = Logger.getLogger(RequestHelper.class
			.getName());

	static final String MAC_PREFIX = "mac";

	/**
	 * Parse input parameters for signal samples.
	 * @param timeStamp
	 * @param req
	 * @param resp
	 * @return list of samples or null on error.
	 */
	public static List<Strength> parseSignalSamples(Date timeStamp,
			HttpServletRequest req, HttpServletResponse resp) {

		List<Strength> listSamples = new ArrayList<Strength>();

		@SuppressWarnings("unchecked")
		Enumeration<String> paramNames = req.getParameterNames();

		int signalStrength = 0;

		// mac 
		
		while (paramNames.hasMoreElements()) {
			String paramName = paramNames.nextElement();

			if (paramName.startsWith(MAC_PREFIX)) {
				String macName = paramName.substring(MAC_PREFIX.length());
				//log.finer(macName);
				try {
					signalStrength = Integer.parseInt(req
							.getParameter(MAC_PREFIX + macName));
					//log.finer(macName + " -- " + signalStrength);
				} catch (NumberFormatException e) {
					Response response = new Response(Response.Status.ERROR,
							"invalid signal strength specified: "
									+ req.getParameter(MAC_PREFIX + macName));

					try {
						resp.getWriter().print(response.toJson());
					} catch (IOException ex) {
						log.finer(ex.getMessage());
					}
					return null;
				}

				Strength so = new Strength();
				so.setDate(timeStamp);
				so.setAddress(macName);
				so.setStrength(signalStrength);

				listSamples.add(so);
			}
		}

		return listSamples;
	}

}
