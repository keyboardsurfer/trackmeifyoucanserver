package org.cbase.trackme.gae;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.cbase.trackme.gae.dao.MemcacheDao;
import org.cbase.trackme.gae.db.Dao;
import org.cbase.trackme.gae.model.Fingerprint;
import org.cbase.trackme.gae.model.Strength;

public class InitDbServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private static final Logger log = Logger.getLogger(InitDbServlet.class
			.getName());

	private Dao dao = new MemcacheDao();

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {

		log.info("doGet");

		for (Fingerprint fp : basicMap()) {

			dao.insert(fp);

		}

		@SuppressWarnings("deprecation")
		List<Fingerprint> entries = (List<Fingerprint>) dao.getFingerprints();

		if (entries.size() > 0) {
			for (Fingerprint l : entries) {

				log.info(l.toJsonObject());
				resp.getWriter().println(l.toJsonObject());

			}
		} else {
			Response response = new Response(Response.Status.WARNING,
					"no rooms");

			resp.getWriter().print(response.toJson());
		}
	}

	private List<Fingerprint> basicMap() {

		ArrayList<Fingerprint> fps = new ArrayList<Fingerprint>();

		long now = System.currentTimeMillis();

		fps.add(basicMapFingerprint("p1", now, "00:00:00:01", -10,
				"00:00:00:02", -20, "00:00:00:03", -30, "00:00:00:04", -20));

		fps.add(basicMapFingerprint("p2", now, "00:00:00:01", -20,
				"00:00:00:02", -10, "00:00:00:03", -20, "00:00:00:04", -30));

		fps.add(basicMapFingerprint("p3", now, "00:00:00:01", -30,
				"00:00:00:02", -20, "00:00:00:03", -10, "00:00:00:04", -20));

		fps.add(basicMapFingerprint("p4", now, "00:00:00:01", -20,
				"00:00:00:02", -30, "00:00:00:03", -20, "00:00:00:04", -10));

		return fps;

	}

	private Fingerprint basicMapFingerprint(String string, long now,
			String string2, int i, String string3, int j, String string4,
			int k, String string5, int l) {

		Fingerprint fp = new Fingerprint();

		fp.setLocation(string);
		fp.setTimeStamp(new Date(now));

		List<Strength> strengths = new ArrayList<Strength>();

		strengths.add(new Strength(string2, i));
		strengths.add(new Strength(string3, j));
		strengths.add(new Strength(string4, k));
		strengths.add(new Strength(string5, l));

		fp.setStrengths(strengths);

		return fp;
	}
}
