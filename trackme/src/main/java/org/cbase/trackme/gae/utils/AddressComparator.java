package org.cbase.trackme.gae.utils;

import org.cbase.trackme.gae.model.Strength;

import java.util.Comparator;

public class AddressComparator implements Comparator<Strength> {

	@Override
	public int compare(Strength arg0, Strength arg1) {
		return arg0.getAddress().compareTo(arg1.getAddress());
	}

}
