package org.cbase.trackme.gae.utils;

import com.google.gson.FieldNamingPolicy;
import com.google.gson.GsonBuilder;
import com.google.gson.annotations.Expose;
import org.cbase.trackme.gae.model.Fingerprint;

/**
 * Value holder.
 * 
 * User: biafra
 * Date: 1/4/12
 * Time: 10:16 PM
 */
public class ConfidenceFingerprintPair {

  @Expose
  private final float       confidence;

  @Expose
  private final Fingerprint fingerprint;

  public ConfidenceFingerprintPair(float confidence, Fingerprint fingerprint) {
    this.confidence = confidence;
    this.fingerprint = fingerprint;
  }

  @Override
  public String toString() {
    return "ConfidenceFingerprintPair{" +
           "confidence=" + confidence +
           ", fingerprint=" + fingerprint +
           '}';
  }

  public float getConfidence() {
    return confidence;
  }

  public Fingerprint getFingerprint() {
    return fingerprint;
  }

  public String json() {
    GsonBuilder builder = new GsonBuilder().setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES);
    return builder.create().toJson(this);
  }
}
