package org.cbase.trackme.gae;

import com.google.gson.Gson;

public class Response {

	public enum Status {
		ERROR, OK, WARNING
	}
	
	private Status status;
	private String message;

	public Response(Status status, String message) {
	    this.status = status;
	    this.message = message;
	}
	
	public String toJson() {
  	Gson gson = new Gson();
  	String json = gson.toJson(this);
  	return json;
  }
	
  public static Response constructFromJson(String json) {
  	Gson gson = new Gson();
  	return gson.fromJson(json, Response.class);
  }

public Status getStatus() {
	return status;
}

public String getMessage() {
	return message;
}
	
}
