package org.cbase.trackme.gae.db;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.logging.Logger;

import org.cbase.trackme.gae.model.Fingerprint;
import org.cbase.trackme.gae.model.Strength;

import com.googlecode.objectify.Objectify;
import com.googlecode.objectify.ObjectifyService;

/**
 * Implementation of a DAO to persist fingerprint objects using the Objectify
 * persistence engine.
 * 
 * User: biafra Date: 1/2/12 Time: 11:04 PM
 * 
 * @deprecated
 * 
 */
public class DaoImpl implements Dao {

	private static final Logger log = Logger.getLogger(DaoImpl.class.getName());

	private static Objectify getService() {
		return ObjectifyService.begin();
	}

	public DaoImpl() {

		// ofy = ObjectifyService.begin();

		// log.info("new instance created: " + ofy);

	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public long insert(Fingerprint fingerprint) {
		return getService().put(fingerprint).getId();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Fingerprint findFingerprintForId(long id) {

		return getService().get(Fingerprint.class, id);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Collection<Fingerprint> getFingerprints() {
		return getService().query(Fingerprint.class).list();
	}


	/**
	 * {@inheritDoc}
	 */
	@Override
	public Collection<Fingerprint> getMatchingFingerprints(
			List<Strength> requestStrengths) {

		ArrayList<Fingerprint> hits = new ArrayList<Fingerprint>();
		
		// write addresses into a set for lookup...
		Set<String> addressLookup = new HashSet<String>();

		for (Strength s : requestStrengths) {
			addressLookup.add(s.getAddress());
		}

		// load all!! existing fingerprints from db (and memcache) 
		Collection<Fingerprint> dbFingerprints = this.getFingerprints();

		log.info(MessageFormat.format("fetched {0} Fingerprints", dbFingerprints.size()));
		
		Iterator<Fingerprint> iter = dbFingerprints.iterator();

		// keep those fingerprints that have a sample for any of the specified mac addresses
		while (iter.hasNext()) {

			Fingerprint f = iter.next();

			//log.info(MessageFormat.format("checking fingerprint: {0} ", f));
			
			for (Strength s : f.getStrengths()) {
				if (addressLookup.contains(s.getAddress())) {
					hits.add(f);
					break; // next fingerprint, please...
				}
			}
		}
		
		log.info(MessageFormat.format("matched {0} Fingerprints", hits.size()));

		return hits;
	}

}
