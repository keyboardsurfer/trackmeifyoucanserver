package org.cbase.trackme.gae;

import java.io.IOException;
import java.util.List;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.cbase.trackme.gae.dao.MemcacheDao;
import org.cbase.trackme.gae.db.Dao;
import org.cbase.trackme.gae.model.Fingerprint;
import org.cbase.trackme.gae.model.Strength;

@SuppressWarnings("serial")
public class SaveLocationServlet extends HttpServlet {

	private static final Logger log = Logger
			.getLogger(SaveLocationServlet.class.getName());

	
	
	private Dao dao = new MemcacheDao();
	
	/**
	 * Insert a new fingerprint into the database. Mandatory request parameters
	 * are LOCATION and a list of samples. Timestamp is optional an will default
	 * to the server date.
	 */
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {

		log.info("SaveLocation - doGet");
		
		SaveLocationArguments args = APICallTranslator.parsteSaveArguments(req, resp); 

		if(args == null) {
			return;
		}
		
	

		// parse input and build list of samples ...
		// TODO: explain format of sample list
		// TODO: Range for strength
		// TODO: parsing of parameters is somewhat redundant. May be parsing the
		// same entry multiple times...
		
		List<Strength> sampleList = APICallTranslator.parseSamples(req, resp);
		
		if(sampleList == null) {
			return;
		}

		
		/*
		 * Ignore fingerprint, if no samples provided..
		 */
		if (sampleList.size() > 0) {
			Fingerprint fingerprint = new Fingerprint();
			fingerprint.setLocation(args.getPosition());
			fingerprint.setTimeStamp(args.getTimestamp());
			fingerprint.setStrengths(sampleList);

			dao.insert(fingerprint);

			Response response = new Response(Response.Status.OK, ":)");

			resp.getWriter().print(response.toJson());

		} else {

			Response response = new Response(Response.Status.ERROR,
					"samples are mandatory");

			resp.getWriter().print(response.toJson());

		}

	}

}
