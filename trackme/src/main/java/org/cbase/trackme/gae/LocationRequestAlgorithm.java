package org.cbase.trackme.gae;

import java.util.List;

import org.cbase.trackme.gae.model.Strength;
import org.cbase.trackme.gae.utils.ConfidenceFingerprintPair;

public interface LocationRequestAlgorithm {

	List<ConfidenceFingerprintPair> sortedMatchingFingerprintsL(List<Strength> requestStrengths);
}
