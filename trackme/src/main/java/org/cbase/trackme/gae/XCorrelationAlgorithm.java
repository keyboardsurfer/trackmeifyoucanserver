package org.cbase.trackme.gae;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import org.cbase.trackme.gae.dao.MemcacheDao;
import org.cbase.trackme.gae.db.Dao;
import org.cbase.trackme.gae.model.Fingerprint;
import org.cbase.trackme.gae.model.Strength;
import org.cbase.trackme.gae.utils.ConfidenceComparator;
import org.cbase.trackme.gae.utils.ConfidenceFingerprintPair;

public class XCorrelationAlgorithm implements LocationRequestAlgorithm {

	private Dao dao = new MemcacheDao();
	
	@Override
	public List<ConfidenceFingerprintPair> sortedMatchingFingerprintsL(
			List<Strength> requestStrengths) {
		
		Collection<Fingerprint> entries = dao
				.getMatchingFingerprints(requestStrengths);

		List<ConfidenceFingerprintPair> result = computeConfidence(
				requestStrengths, entries);
		
		return result;
	}

	protected List<ConfidenceFingerprintPair> computeConfidence(
			List<Strength> requestStrengths, Collection<Fingerprint> entries) {
		
		
		ArrayList<ConfidenceFingerprintPair> result = new ArrayList<ConfidenceFingerprintPair>();
		
		for(Fingerprint f : entries) {
			
			
			float confidence = correlate(requestStrengths, f.getStrengths());
			
			result.add(new ConfidenceFingerprintPair(confidence, f));
		}
		
		Collections.sort(result,new ConfidenceComparator());
		
		return result;
	}

	private float correlate(List<Strength> requestStrengths,
			List<Strength> strengths) {
		
		
		// lower bound for deltas
		float DELTA_MIN = 0.01F;
		
		// upper bound for the confidence value (so that we can scale to a value betweeen 1 and 100
		//float CONFIDENCE_MAX = 1/DELTA_MIN;
		
		// first collect one side into a map
		Map<String,Long> h1 = new Hashtable<String,Long>();
		
		for(Strength s : strengths) {
			h1.put(s.getAddress(), new Long(s.getStrength()));
		}
		
		// now iterate and add correlation..
		int summand_count = 0;
		float xc = 0;
		for(Strength s : requestStrengths) {
			
			Long signal = h1.get(s.getAddress());
			if(signal != null)  {
				
				float delta = Math.abs(signal - s.getStrength());
				
				if(delta < DELTA_MIN) {
					delta = DELTA_MIN;
				}
				
				xc = xc + 1/delta;
				summand_count++;
			}
			
		}
		
		return xc/summand_count;
	}

	

}
