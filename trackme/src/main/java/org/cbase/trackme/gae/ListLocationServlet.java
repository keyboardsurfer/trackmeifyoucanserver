package org.cbase.trackme.gae;

import java.io.IOException;
import java.util.List;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.cbase.trackme.gae.dao.MemcacheDao;
import org.cbase.trackme.gae.db.Dao;
import org.cbase.trackme.gae.model.Fingerprint;

/**
 * Retrieves all fingerprint entities from the database.
 * 
 * @deprecated
 *  
 * @author mmachicao
 *
 */
@SuppressWarnings("serial")
public class ListLocationServlet extends HttpServlet {

  private static final Logger log = Logger.getLogger(ListLocationServlet.class.getName());
//  private              Dao    dao = new DaoImpl();

  private              Dao    dao = new MemcacheDao();
  
  @Override
  protected void doGet(HttpServletRequest req, HttpServletResponse resp)
    throws ServletException, IOException {

    log.info("doGet");

    List<Fingerprint> entries = (List<Fingerprint>) dao.getFingerprints();

    if (entries.size() > 0) {
      for (Fingerprint l : entries) {

        log.info(l.toJsonObject());
        resp.getWriter().println(l.toJsonObject());

      }
    } else {
      Response response = new Response(Response.Status.WARNING, "no rooms");

      resp.getWriter().print(response.toJson());
    }
  }
}