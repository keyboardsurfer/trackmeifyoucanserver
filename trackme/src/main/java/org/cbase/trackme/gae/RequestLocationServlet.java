package org.cbase.trackme.gae;

import java.io.IOException;
import java.text.MessageFormat;
import java.util.List;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.cbase.trackme.gae.model.Strength;
import org.cbase.trackme.gae.utils.ConfidenceFingerprintPair;

import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

@SuppressWarnings("serial")
public class RequestLocationServlet extends HttpServlet {

	private static final Logger log = Logger
			.getLogger(RequestLocationServlet.class.getName());

	
	//private LocationRequestAlgorithm algorithm = new BasicLocationAlgorithm();
	
	private LocationRequestAlgorithm algorithm = new XCorrelationAlgorithm();

	// create and configure a gson builder
	private Gson gson = new GsonBuilder()
			// .registerTypeAdapter(Date.class, new DateSerializer())
			.setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
			.excludeFieldsWithoutExposeAnnotation().serializeNulls().create();

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {

		// Step 1 : identify input
		log.info("doGet");
		
		List<Strength> sampleList = RequestHelper.parseSignalSamples(null, req, resp);

		
		if(sampleList == null) {
			log.info("failed to parse list of samples");
			return;
		} else {
			log.info(MessageFormat.format("parsed {0} samples",sampleList.size()));
		}
		
		// use samples to identify stored fingerprints in the proximity
		List<ConfidenceFingerprintPair> result = algorithm
				.sortedMatchingFingerprintsL(sampleList);

		// return at most best 10 matches
		List<ConfidenceFingerprintPair> subList;
		if (result.size() > 10) {
			subList = result.subList(0, 10);
		} else {
			subList = result;
		}

		final String json = gson.toJson(subList);
		System.out.println("json: " + json);
		resp.getWriter().print(json);
		
		
		resp.getWriter().flush();

	}


}
