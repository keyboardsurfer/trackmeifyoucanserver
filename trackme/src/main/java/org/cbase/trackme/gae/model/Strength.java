package org.cbase.trackme.gae.model;

import com.google.gson.FieldNamingPolicy;
import com.google.gson.GsonBuilder;
import com.googlecode.objectify.annotation.Cached;

import java.util.Date;

/**
 * Representation of a sample as recorded by a device
 * 
 * User: biafra
 * Date: 12/29/11
 * Time: 10:39 PM
 */
@Cached  // TODO: explain why it (is /is not) required to keep this..
public class Strength implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * address of the sampled device 
	 */
  String address;

  /**
   * Strength of the signal received from the device
   */
  Integer strength;

  /**
   * The date when the sample was taken
   * 
   * TODO: Consider removing this value, as the instance is always associated to a fingerprint.
   */
  Date             date;

  public Strength() {

  }

  public Strength(String address, int strength) {
    this.address = address;
    this.strength = strength;
  }

  public String getAddress() {
    return address;
  }

  public void setAddress(String address) {
    this.address = address;
  }

  public Integer getStrength() {
    return strength;
  }

  public void setStrength(Integer strength) {
    this.strength = strength;
  }

  public Date getDate() {
    return date;
  }

  public void setDate(Date date) {
    this.date = date;
  }


  @Override
  public String toString() {
    return "Strength{" +
           "address='" + address + '\'' +
           ", strength=" + strength +
           ", date=" + date +
           '}';
  }

  public String toJson() {
    GsonBuilder builder = new GsonBuilder().setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES);
    return builder.create().toJson(this);
  }
}
