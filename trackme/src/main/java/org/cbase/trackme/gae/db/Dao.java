package org.cbase.trackme.gae.db;

import java.util.Collection;
import java.util.List;

import org.cbase.trackme.gae.model.Fingerprint;
import org.cbase.trackme.gae.model.Strength;

/**
 * User: biafra
 * Date: 1/2/12
 * Time: 11:04 PM
 */
public interface Dao {

	/**
	 * Persist a new fingerprint in the database
	 * @param fingerprint to persist
	 * @return
	 */
  public long insert(Fingerprint fingerprint);

  /**
   * Read a fingerprint having the specified id value
   * 
   * @param id to use
   * @return
   */
  public Fingerprint findFingerprintForId(long id);

  /**
   * Return all available fingerprints 
   * @deprecated
   * @return
   */
  public Collection<Fingerprint> getFingerprints();
  
  /**
   * Select only those fingerprints that have at least one MAC address in common
   * 
   * @param fingerprint
   * @return
   */
  public Collection<Fingerprint> getMatchingFingerprints(List<Strength> requestStrengths);
}
