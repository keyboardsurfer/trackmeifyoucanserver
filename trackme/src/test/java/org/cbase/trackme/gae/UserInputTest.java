package org.cbase.trackme.gae;

import junit.framework.TestCase;

import java.util.Date;


public class UserInputTest extends TestCase {

	
	public void testDateInputValid() {
	
		String input = Long.toString(System.currentTimeMillis());
		
		Date timeStamp = new Date(Long.parseLong(input));
		
		assertTrue(timeStamp != null);
	}
	
	public void testDateInputInvalid() {
		
		String input = "this is definetly not a decimal number!";
		
		try {
		new Date(Long.parseLong(input));
		
		fail("I expect an exception to be thrown");
		} catch (NumberFormatException e){
			// ok...
		} catch (Throwable t) {
			fail("I don't expect any other type of throwable");
		}
		
		
	}

	public void testDateInputRangeInvalid() {
		
		String input = Long.toString(-1 * System.currentTimeMillis());
		
		Date timeStamp = new Date(Long.parseLong(input));
		
		assertTrue(timeStamp != null);
		
		assertTrue(timeStamp.getTime() < 0);
	}
	
}
