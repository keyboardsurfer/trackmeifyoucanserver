package org.cbase.trackme.gae.utils;

import java.util.ArrayList;
import java.util.Collections;

import org.cbase.trackme.gae.model.Fingerprint;

import junit.framework.TestCase;

public class ConfidenceComparatorTest extends TestCase {

	private static String A = "a";

	private static String B = "b";
	
	private static String C = "c";

	public void testEqual() {

		ArrayList<ConfidenceFingerprintPair> ab = new ArrayList<ConfidenceFingerprintPair>();

		Fingerprint fa = new Fingerprint();
		fa.setLocation(A);

		Fingerprint fb = new Fingerprint();
		fb.setLocation(B);

		ConfidenceFingerprintPair a = new ConfidenceFingerprintPair(0, fa);
		ConfidenceFingerprintPair b = new ConfidenceFingerprintPair(0, fb);

		ab.add(0, a);
		ab.add(1, b);

		Collections.sort(ab, new ConfidenceComparator());

		// check that the position of the entries has not changed
		assertTrue(ab.get(0).getFingerprint().getLocation().equals(A));
		assertTrue(ab.get(1).getFingerprint().getLocation().equals(B));

	}

	public void testABC() {

		ArrayList<ConfidenceFingerprintPair> ab = new ArrayList<ConfidenceFingerprintPair>();

		Fingerprint fa = new Fingerprint();
		fa.setLocation(A);

		Fingerprint fb = new Fingerprint();
		fb.setLocation(B);

		Fingerprint fc = new Fingerprint();
		fc.setLocation(C);

		ConfidenceFingerprintPair a = new ConfidenceFingerprintPair(50, fa);
		ConfidenceFingerprintPair b = new ConfidenceFingerprintPair(25, fb);
		ConfidenceFingerprintPair c = new ConfidenceFingerprintPair(75, fc);

		ab.add(0, a);
		ab.add(1, b);
		ab.add(2, c);
		
		Collections.sort(ab, new ConfidenceComparator());

		// Attention: greater confidence value is better
		assertTrue(ab.get(0).getFingerprint().getLocation().equals(C));
		assertTrue(ab.get(1).getFingerprint().getLocation().equals(A));
		assertTrue(ab.get(2).getFingerprint().getLocation().equals(B));

	}

}
