package org.cbase.trackme.gae;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.cbase.trackme.gae.model.Fingerprint;
import org.cbase.trackme.gae.model.Strength;
import org.cbase.trackme.gae.utils.ConfidenceFingerprintPair;
import org.junit.Test;

public class XCorrelationAlgorithmTest {

	float ERROR_MARGIN = 0.1F;

	private XCorrelationAlgorithm alg = new XCorrelationAlgorithm();

	@Test
	public void testP2_P2() {

		List<Strength> requestStrengths = new ArrayList<Strength>();

		requestStrengths.add(new Strength("00:00:00:02", 10));

		List<ConfidenceFingerprintPair> fppl = alg.computeConfidence(
				requestStrengths, basicMap());

		assertTrue(fppl != null);

		ConfidenceFingerprintPair f = getBestPair(fppl);

		assertEquals("p2", f.getFingerprint().getLocation());

		// check that the confidence is 100.
		assertTrue(Math.abs(100 - f.getConfidence()) < ERROR_MARGIN);

	}

	@Test
	public void testP2_P4() {

		List<Strength> requestStrengths = new ArrayList<Strength>();

		requestStrengths.add(new Strength("00:00:00:02", 30));

		List<ConfidenceFingerprintPair> fppl = alg.computeConfidence(
				requestStrengths, basicMap());

		assertTrue(fppl != null);

		ConfidenceFingerprintPair f = getBestPair(fppl);

		assertEquals("p4", f.getFingerprint().getLocation());

		// check that the confidence is 100.
		assertTrue(Math.abs(100 - f.getConfidence()) < ERROR_MARGIN);

	}
	
	@Test
	public void testP2_P1OrP3() {

		List<Strength> requestStrengths = new ArrayList<Strength>();

		requestStrengths.add(new Strength("00:00:00:02", 20));

		List<ConfidenceFingerprintPair> fppl = alg.computeConfidence(
				requestStrengths, basicMap());

		assertTrue(fppl != null);

		ConfidenceFingerprintPair f = getBestPair(fppl);

		boolean match = f.getFingerprint().getLocation().equals("p1") | f.getFingerprint().getLocation().equals("p3");
		
		assertTrue(match);

	}
	

	@Test
	public void testP1234_P1() {

		List<Strength> requestStrengths = new ArrayList<Strength>();

		requestStrengths.add(new Strength("00:00:00:01", 20));
		requestStrengths.add(new Strength("00:00:00:02", 10));
		requestStrengths.add(new Strength("00:00:00:03", 20));
		requestStrengths.add(new Strength("00:00:00:04", 30));

		List<ConfidenceFingerprintPair> fppl = alg.computeConfidence(
				requestStrengths, basicMap());

		assertTrue(fppl != null);

		ConfidenceFingerprintPair f = getBestPair(fppl);

		assertEquals("p2", f.getFingerprint().getLocation());

		// check that the confidence is 100.
		assertTrue(Math.abs(100 - f.getConfidence()) < ERROR_MARGIN);

	}
	
	
	@Test
	public void testUpperLeftOfP1_P1() {

		List<Strength> requestStrengths = new ArrayList<Strength>();

		requestStrengths.add(new Strength("00:00:00:01", 15));
		requestStrengths.add(new Strength("00:00:00:02", 25));
		requestStrengths.add(new Strength("00:00:00:03", 35));
		requestStrengths.add(new Strength("00:00:00:04", 25));

		List<ConfidenceFingerprintPair> fppl = alg.computeConfidence(
				requestStrengths, basicMap());

		assertTrue(fppl != null);

		ConfidenceFingerprintPair f = getBestPair(fppl);

		assertEquals("p1", f.getFingerprint().getLocation());

	}


	@Test
	public void testLowerRightOfP1_P1() {

		List<Strength> requestStrengths = new ArrayList<Strength>();

		requestStrengths.add(new Strength("00:00:00:01", 15));
		requestStrengths.add(new Strength("00:00:00:02", 15));
		requestStrengths.add(new Strength("00:00:00:03", 25));
		requestStrengths.add(new Strength("00:00:00:04", 15));

		List<ConfidenceFingerprintPair> fppl = alg.computeConfidence(
				requestStrengths, basicMap());

		assertTrue(fppl != null);

		for (ConfidenceFingerprintPair p : fppl) {
			System.out.println(p);
		}

		ConfidenceFingerprintPair f = getBestPair(fppl);

		assertEquals("p1", f.getFingerprint().getLocation());

	}


	private ConfidenceFingerprintPair getBestPair(
			List<ConfidenceFingerprintPair> fppl) {

		float bestConfidence = -1;

		ConfidenceFingerprintPair bestPair = null;

		for (ConfidenceFingerprintPair p : fppl) {

			if (p.getConfidence() > bestConfidence) {
				bestConfidence = p.getConfidence();
				bestPair = p;

			}
		}

		return bestPair;
	}

	private List<Fingerprint> basicMap() {

		ArrayList<Fingerprint> fps = new ArrayList<Fingerprint>();

		long now = System.currentTimeMillis();

		int factor = -1;

		fps.add(basicMapFingerprint("p1", now, "00:00:00:01", -10,
				"00:00:00:02", -20, "00:00:00:03", -30, "00:00:00:04", -20,
				factor));

		fps.add(basicMapFingerprint("p2", now, "00:00:00:01", -20,
				"00:00:00:02", -10, "00:00:00:03", -20, "00:00:00:04", -30,
				factor));

		fps.add(basicMapFingerprint("p3", now, "00:00:00:01", -30,
				"00:00:00:02", -20, "00:00:00:03", -10, "00:00:00:04", -20,
				factor));

		fps.add(basicMapFingerprint("p4", now, "00:00:00:01", -20,
				"00:00:00:02", -30, "00:00:00:03", -20, "00:00:00:04", -10,
				factor));

		return fps;

	}

	private Fingerprint basicMapFingerprint(String string, long now,
			String string2, int i, String string3, int j, String string4,
			int k, String string5, int l, int factor) {

		Fingerprint fp = new Fingerprint();

		fp.setLocation(string);
		fp.setTimeStamp(new Date(now));

		List<Strength> strengths = new ArrayList<Strength>();

		strengths.add(new Strength(string2, factor * i));
		strengths.add(new Strength(string3, factor * j));
		strengths.add(new Strength(string4, factor * k));
		strengths.add(new Strength(string5, factor * l));

		fp.setStrengths(strengths);

		return fp;
	}

}
