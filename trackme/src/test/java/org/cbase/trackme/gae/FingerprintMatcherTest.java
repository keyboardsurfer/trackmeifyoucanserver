package org.cbase.trackme.gae;

import junit.framework.TestCase;
import org.cbase.trackme.gae.model.Strength;
import org.junit.Before;

import java.util.ArrayList;
import java.util.List;

/**
 * User: biafra
 * Date: 1/4/12
 * Time: 7:36 PM
 */
public class FingerprintMatcherTest extends TestCase {

  @Override
  @Before
  public void setUp() {
    //System.out.println("FOOOOOOOO test");
  }

  public void testCompareStrengthNull() {
    System.out.println("testCompareStrengthNull");
    float result = BasicLocationAlgorithm.compareStrength(null, null);
    assertEquals(100.0F, result);

  }


  public void testCompareStrengthEmpty() {
    System.out.println("testCompareStrengthEmpty");
    float result = BasicLocationAlgorithm.compareStrength(new ArrayList<Strength>(), new ArrayList<Strength>());
    assertEquals(100.0F, result);
  }


  public void testCompareStrengthSame() {
    System.out.println("testCompareStrength");
    final ArrayList<Strength> list1 = new ArrayList<Strength>();
    list1.add(new Strength("a", 1));
    list1.add(new Strength("b", 10));
    list1.add(new Strength("c", 11));

    final ArrayList<Strength> list2 = new ArrayList<Strength>();
    list2.add(new Strength("a", 1));
    list2.add(new Strength("b", 10));
    list2.add(new Strength("c", 11));

    float result = BasicLocationAlgorithm.compareStrength(list1, list2);

    assertEquals(0.0F, result);
  }

  public void testCompareStrengthList1Smaller() {
    System.out.println("testCompareStrengthList1Smaller");
    final ArrayList<Strength> list1 = new ArrayList<Strength>();
    list1.add(new Strength("a", 20));
    list1.add(new Strength("b", 20));

    final ArrayList<Strength> list2 = new ArrayList<Strength>();
    list2.add(new Strength("a", 10));
    list2.add(new Strength("b", 10));
    list2.add(new Strength("c", 10));

    float result = BasicLocationAlgorithm.compareStrength(list1, list2);

    assertEquals(110.0F / 3, result);
  }

  public void testCompareStrengthList2Smaller() {
    System.out.println("testCompareStrengthList2Smaller");
    final ArrayList<Strength> list1 = new ArrayList<Strength>();
    list1.add(new Strength("a", 20));
    list1.add(new Strength("b", 20));
    list1.add(new Strength("c", 20));

    final ArrayList<Strength> list2 = new ArrayList<Strength>();
    list2.add(new Strength("a", 10));
    list2.add(new Strength("b", 10));

    float result = BasicLocationAlgorithm.compareStrength(list1, list2);

    assertEquals(100.0F / 3, result);
  }

  public void testCompareStrengthListsDiff1() {
    System.out.println("testCompareStrengthList2Smaller");
    final ArrayList<Strength> list1 = new ArrayList<Strength>();
    list1.add(new Strength("a", 10));
    list1.add(new Strength("b", 10));
    list1.add(new Strength("c", 10));

    final ArrayList<Strength> list2 = new ArrayList<Strength>();
    list2.add(new Strength("a", 10));
    list2.add(new Strength("b", 10));
    list2.add(new Strength("d", 10));

    float result = BasicLocationAlgorithm.compareStrength(list1, list2);

    assertEquals(180.0F / 4, result);
  }

  public void testCompareStrengthListsDiff2() {
    System.out.println("testCompareStrengthListsDiff2");
    final ArrayList<Strength> list1 = new ArrayList<Strength>();
    list1.add(new Strength("a", 10));
    list1.add(new Strength("b", 10));

    final ArrayList<Strength> list2 = new ArrayList<Strength>();
    list2.add(new Strength("a", 10));
    list2.add(new Strength("b", 10));
    list2.add(new Strength("c", 10));
    list2.add(new Strength("d", 10));
    list2.add(new Strength("e", 10));
    list2.add(new Strength("f", 10));

    float result = BasicLocationAlgorithm.compareStrength(list1, list2);

    assertEquals(4 * 90.0F / 6, result);
    // or? assertEquals(180.0F / 4, result);
  }

  public void testCompareStrengthListsDiff3() {
    System.out.println("testCompareStrengthListsDiff2");
    final ArrayList<Strength> list1 = new ArrayList<Strength>();
    list1.add(new Strength("a", 10));
    list1.add(new Strength("b", 10));
//    list1.add(new Strength("c", 100));
//    list1.add(new Strength("d", 100));
//    list1.add(new Strength("e", 100));
//    list1.add(new Strength("f", 100));
    list1.add(new Strength("g", 10));
    list1.add(new Strength("h", 10));
    list1.add(new Strength("i", 10));
    list1.add(new Strength("j", 10));

    final ArrayList<Strength> list2 = new ArrayList<Strength>();
    list2.add(new Strength("a", 10));
    list2.add(new Strength("b", 10));
    list2.add(new Strength("c", 10));
    list2.add(new Strength("d", 10));
    list2.add(new Strength("e", 10));
    list2.add(new Strength("f", 10));

    float result = BasicLocationAlgorithm.compareStrength(list1, list2);

//    assertEquals(60.0f, result);
    assertEquals(72.0f, result);

  }


  public void testMergeStrengths() {

    final ArrayList<Strength> list1 = new ArrayList<Strength>();
    list1.add(new Strength("a", 10));
    list1.add(new Strength("b", 10));
//    list1.add(new Strength("c", 100));
//    list1.add(new Strength("d", 100));
//    list1.add(new Strength("e", 100));
//    list1.add(new Strength("f", 100));
    list1.add(new Strength("g", 10));
    list1.add(new Strength("h", 10));
    list1.add(new Strength("i", 10));
    list1.add(new Strength("j", 10));

    final ArrayList<Strength> list2 = new ArrayList<Strength>();
    list2.add(new Strength("a", 10));
    list2.add(new Strength("b", 10));
    list2.add(new Strength("c", 10));
    list2.add(new Strength("d", 10));
    list2.add(new Strength("e", 10));
    list2.add(new Strength("f", 10));

    List<BasicLocationAlgorithm.AddressStrengthPair> result = BasicLocationAlgorithm.mergeStrengths(list1, list2);
    assertEquals(10, result.size());

    assertEquals("a", result.get(0).address);
    assertEquals(10, result.get(0).strength1);
    assertEquals(10, result.get(0).strength2);

    assertEquals("b", result.get(1).address);
    assertEquals(10, result.get(1).strength1);
    assertEquals(10, result.get(1).strength2);

    assertEquals("c", result.get(2).address);
    assertEquals(100, result.get(2).strength1);
    assertEquals(10, result.get(2).strength2);

    assertEquals("d", result.get(3).address);
    assertEquals(100, result.get(3).strength1);
    assertEquals(10, result.get(3).strength2);

    assertEquals("e", result.get(4).address);
    assertEquals(100, result.get(4).strength1);
    assertEquals(10, result.get(4).strength2);

    assertEquals("f", result.get(5).address);
    assertEquals(100, result.get(5).strength1);
    assertEquals(10, result.get(5).strength2);

    assertEquals("g", result.get(6).address);
    assertEquals(10, result.get(6).strength1);
    assertEquals(100, result.get(6).strength2);

    assertEquals("h", result.get(7).address);
    assertEquals(10, result.get(7).strength1);
    assertEquals(100, result.get(7).strength2);

    assertEquals("i", result.get(8).address);
    assertEquals(10, result.get(8).strength1);
    assertEquals(100, result.get(8).strength2);

    assertEquals("j", result.get(9).address);
    assertEquals(10, result.get(9).strength1);
    assertEquals(100, result.get(9).strength2);
  }
}
